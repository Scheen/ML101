# Logistic Regression on the Titanic Dataset

The Titanic Dataset is probably the most well known machine learning exercise. It comes with a dataset that consists of Titanic passengers and their characteristics (e.g. age, gender, travel class,...). It also contains a variable that tells us whether or not the passenger survived the Titanic tradegy. The ultimate goal of the exercise is to predict survival based on passenger characteristics.

We will try to built a Logistic Regression classifier in Python to make such predicitions. The following exercise will guide you step-by-step through the modelling process. 


## Step 0: Import packages 

Before we start, we need to load a couple of packages that we will need throughout the exercise. 

```python
import pandas as pd
import os as os
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.cross_validation import train_test_split
```

## Step 1:  Load your data

It's time to load the titanic datasets. Two files (test.csv and train.csv) are at your disposal. The train file is for model training. It will teach the model if and how some features influence the outcome variable (target). The test set is for model validation. It contains data that the model has not seen during training. If we let the model predict the outcomes of this dataset, we get a sound idea about how good our model understands the relationship between features and the target.  

```python
os.chdir("/home/max/Data_ML_exercices/")

df_train = pd.read_csv("train.csv")
df_test = pd.read_csv("test.csv")
```

Now, we need to define target and add it to the target test file. This is necessary so that the classification alorythm can learn from the outcomes.  

```python
df_test_target = pd.read_csv("gender_submission.csv")
df_test = df_test.join(df_test_target, rsuffix='_right')
```

## Step 2: Take look at your data

To get an overview of your data, check the first rows of your data, and print some summary tables. You can call [.head()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.head.html) and [.info()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.info.html?highlight=info#pandas.DataFrame.info) on the data frames to do that. 

```python
df_train.head() # check the first rows of the data frame
df_test.head()

df_train.info() # summary statistics of the data frame
df_test.info()
```

The feature 'Cabin' has many missing values.
The feature 'Ticket' doesn’t add much value to our analysis.

## Step 3: Data pre-processing 

##### Drop useless variables

Remove the 'useless' variables 'Ticket and 'Cabin' from your data frames. You can also delete "Name" and "PassengerId". The method [.drop()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.drop.html?highlight=drop#pandas.DataFrame.drop) will help you doing this. 

```python
df_train = df_train.drop(["Ticket","Cabin", "Name", "PassengerId"], axis =1) # drop() removes one or more columns
df_test = df_test.drop(["Ticket","Cabin", "Name", "PassengerId"], axis =1)
```

##### Replace missing values 

The remaining variables will be used for modelling. However, the column 'Age' contain missing values. Instead of just removing them, we could  replace them with our best guess. Use [.fillna()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.fillna.html?highlight=fillna#pandas.DataFrame.fillna) on the 'Age' column to replace missing values with the [.median()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.median.html?highlight=median). 

```python
df_train["Age"]  = df_train["Age"].fillna(df_train["Age"].median()) # replace missing values with the age median
df_test["Age"]  = df_test["Age"].fillna(df_test["Age"].median())
df_test = df_test.dropna()
```

##### Take another look at the data to verify your changes 

Check the results of your data cleaning with [.info()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.info.html?highlight=info#pandas.DataFrame.info). Also, [.count()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.count.html) the number of survivers/casualities in the test set. 

```python
df_train.info() 
df_test.info()

df_test.groupby("Survived").count() #Passengers that survived vs. passengers that passed away
```

##### Create dummy variables for the logistic regression

The 'Pclass' indentifies the passengers' travel class (1st to 3rd). It is represented by numbers ranging from 1 to 3. We need to tell the program that these represent groups and not continuous numbers. Convert the column 'Pclass' to type 'catgeory' using [.astype()](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.astype.html?highlight=astype#pandas.DataFrame.astype).     

```python
df_train["Pclass"]  = df_train["Pclass"].astype("category") # change data type of Pclass from integer to category
df_test["Pclass"]  = df_test["Pclass"].astype("category")
```

Now that the program understands the real meaning of "Pclass" values, we can create dummies from it. Below, you can see how this is done for "Embarked", "Sex" and "Pclass". Run this code and check the printed results. Do you understand the pattern?

```python
dummies = pd.get_dummies(df_train[["Embarked", "Sex", "Pclass"]]) # Create dummy encoding 
dummies_test = pd.get_dummies(df_test[["Embarked", "Sex", "Pclass"]])

dummies.info() # Take a look at your dummies
print(dummies)
print(dummies_test)
```

##### Define the feature set X

Next, we are going to prepare the feature table, which we will use to model the survival. 
	1. Create 2 new data frames 'X_train' for train and 'X_test' for test. Remove the variables that we encoded as dummies.
	2. Add the corrsponding dummies to both data frames.

```python
X = df_train.drop(["Embarked", "Sex", "Pclass"], axis =1) # Drop the column for which we created dummy variables
X_ = df_test.drop(["Embarked", "Sex", "Pclass"], axis =1)

X_train = pd.concat([ X , dummies], axis =1) # Add dummies to your feature set
X_test = pd.concat([ X_ , dummies_test], axis =1)
```




```python
X_test = X_test.drop("PassengerId_right", axis=1) # Drop PassengerId_right columns 
X_test = X_test.dropna() # Drop missing values
X_test.info()  # Take a look at your test set

features_array = X_train.drop(["Survived"],axis =1).values
features_array_test = X_test.drop(["Survived"],axis =1).values

target = df_train["Survived"].values
target_test = df_test["Survived"].dropna().values
```


## Step 4: Modelling 

Remember that we imported a bunch of 'sklearn' packages at the very beginning? Well, we are finally ready to use them. ['Sklearn'](http://scikit-learn.org/stable/) is a Python library with many machine learning algorithms. [LogisticRegression()](http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html) is the classifier algorithm that we will use to train the model. 

##### Fit the logistic regression model
```python
logreg = LogisticRegression()
model = logreg.fit(features_array, target) # Fits model according to the given training data (features + target) 
```

##### Predict your model 
```python
predictions = model.predict(features_array_test) # Predicts class labels for samples in X
print(predictions)
```

## Step 5: Model Evaluation

The next step is to assess the model quality. Much better than looking at the individual predictions is a [confusion matrix](https://en.wikipedia.org/wiki/Confusion_matrix). That is a 2x2 table that displays the model predictions against the observations. The four cells give an overview over... 

	1. True positives (TP): Our classifier predicited survival and the passenger survived in reality.
	2. True negatives (TN): Our classifier predicited death and the passenger died in reality.
	3. False positives (FP): Our classifier predicited survival and the passenger died in reality. 
	4. False negatives (FN): Our classifier predicited death and the passenger survived in reality.

##### Compute confusion matrix
```python
print(confusion_matrix(target_test, predictions)) 
```

##### Compute the accuracy of the model

Take a look at the [confusion matrix](https://en.wikipedia.org/wiki/Confusion_matrix) and create the variables TP, TN, FP and FN with the values displayed in the matrix.

```python
TP = 253.0 # True positives
TN = 141.0 # True negatives
FP = 11.0 # False positives
FN = 12.0 # False negatives
```

Then run the below code to generate the evaluation measures [accuracy](https://en.wikipedia.org/wiki/Accuracy_and_precision#In_binary_classification), [precision and recall](https://en.wikipedia.org/wiki/Precision_and_recall). 

```python
accuracy = (TP+TN)/(TP+TN+FP+FN)
prec_died = TP/(TP+FP)
prec_surv =  TN/(TN+FN)
rec_died =  TP/(TP+FN)
rec_surv = TN/(TN+FP)

print("Accuracy: " + str(accuracy))
print("Precision died: " + str(prec_died))
print("Precision survived: " + str(prec_surv))
print("Recall died: " + str(rec_died)) 
print("Recall survived: " + str(rec_surv) )
```

