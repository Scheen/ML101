# BASIC COMMANDS:

## BASH commands for MobaXTerm
highlight text                                      #>> copy to clipboard 
shift + insert OR right-mouseclick                  #>> paste from clipboard
CRTL + Z                                            #>> move from pyspark, scala to background
fg + ENTER                                          #>> move back to foreground 
```bash
vi .bashrc                                          #>> view existing aliases
. .bashrc                                           #>> refresh bashrc
```

## Text Editor (VIM and Nano) manipulations
```bash
vi .bashrc                      #>> look at aliases
. .bashrc                       #>> refresh bashrc
vi exmFile                      #>> edit file in vim
Esc + :wq                       #>> save and quit vim 
Esc + :q!                       #>> quit vim wihtout saving
```

## Linux commands
```bash
pwd                             #>> get your current directory
ls -l                           #>> check folders in directory with creation date
cd /example/path/               #>> go to directory
cd ..                           #>> move one level up in directory
cd                              #>> go to home directory
nano exmfile                    #>> edit file in nano editor
ctrl+X                          #>> quit nano
```

## HDFS manipulations
```bash
hdfs dfs -mkdir -p /example/path/                   #>> adds new folders (recursive if dir not exist)
hdfs dfs -rm /example/path/table.txt                #>> remove file from hdfs
hdfs dfs -rm (--skipTrash) /example/path/table.txt  #>> hard remove from hdfs
hdfs dfs -ls /example/path/                         #>> check if folder exists
hdfs dfs -get /example/path/table.tx                #>> retrieve file from hdfs and save it to home directory /home/CORPORATEKEY/
hdfs dfs -put /home/CORPORATEKEY/table.csv /example/path/test.csv #>> save table.csv in /example/path/

#>> unzip a zipped hdfs file manually 
mkdir tmp # create temporary local folder
cd ./tmp # go to folder
hdfs dfs -get /data/BE/xfb/SND_pan_trans_10DEC18_16DEC18.zip # load hdfs file into tmp directory
unzip SND_pan_trans_10DEC18_16DEC18.zip # unzip it locally
hdfs dfs -put -f ./pan_trans_10DEC18_16DEC18.csv /data/BE/shared/pan_transactions/pan_trans_10DEC18_16DEC18.csv # put it on a folder of your choice in hdfs
```

## Scala 
```scala
spark-latest                                       //>> start scala session
:quit                                              //>> leave scala session
```

## Pyspark
```python
pyspark                                             #>> start pyspark session
quit()                                              #>> leave pyspark session
spark.sql("show tables in exmDatabase").show()      #>> check tables in DB
spark.table("exmDatabase.exmTable").printSchema()   #>> check variables & types
spark.sql("desc formatted exmDatabase.exmTable").show()  #>> checks the meta data of a table
spark.sql("drop table exmDatabase.exmTable")        #>> remove table from DB
df.toPandas().to_csv('/home/CORPORATEKEY/table.csv')#>> save df to home directory
df.write.mode('overwrite').saveAsTable('exmDatabase.exmTable') #>> save df to hive 
```

## Screen Manipulations
```bash
screen -S exmScreenName                 #>> create new screen
screen -R ScreenNumber                  #>> to reload an existing screen
screen -D                               #>> detach screen
screen -ls                              #>> list all the screens
screen -X -S ScreenNumber quit          #>> kill a screen
```

## Git bash commands 
```bash
git status                                             #>> check sync status 
git checkout -b exmBranchName                          #>> create new branch
git add exmFile                                        #>> add exmFile to tracked files
git commit -m 'exmMessage'                             #>> commit changes
git push origin exmBranchName                          #>> push to Git repo
```












