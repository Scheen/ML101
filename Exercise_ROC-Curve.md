
# Receiver operating characteristic (ROC)

In statistics, a receiver operating characteristic (ROC) is a graphical plot that illustrates the diagnostic ability of a binary classifier. It allows to select possibly optimal models and to discard suboptimal ones. In the following exercise, we are going to look at a binary classifier to illustrate the concept of a ROC curve. 

A classifier maps instances to certain groups or classes. Let's consider a binary prediction problem, where the classifier tries to predict whether a person has a certain disease. It labels each instance either as positive (i.e. it expects the disease to be present) or negative (i.e. it expects the disease to be absent). 

Suppose we know the health status of our observations. There are 4 possible outcomes from the classification: 

	1. True positive: The classifier labels "positive" and the instance is "positive" in reality.
	2. True negative: The classifier labels "negative" and the instance is "negative" in reality.
	3. False positive: The classifier labels "positive", but the instance is not "positive" in reality. 
	4. False negative: The classifier labels "negative", but the instance is not "negative" in reality.


---[Add confusion matrix]---
---[Add Sample Data and instructions to classifiy into the case 1-4]---

Looking at the confusion matrix, we can derive two evaluation metrics that are essential for the ROC curve: the true positive rate (TPR) and false positive rate (FPR). They get calculated as follows: 


---[Add formulas]---
 TPR: sum(True positives)/Sum(condition positive)
 FPR: sum(false positives)/Sum(condition negative)

TPR measures the proportion of positives that are correctly identified as such (i.e. the percentage of sick people who are correctly identified as having the condition). It represents the probability of disease detection.
FPR defines how many incorrect positive results occur among all negative samples. It represents the probability of false alarm.


# Roc Space

The ROC graph plots the true positive rate (Y-axis) against the false positive rate (X-axis). 

## Exercise: 
Suppose you developed the perfect classifier and calculated the TPR and FPR. Where in would the ROC curve of such a model have?

	A1: The space between the two axis depicts relative trade-offs between true positive (benefits) and false positive (costs). The best possible prediction method would yield a point in the upper left corner or coordinate (0,1) of the ROC space, representing 100% sensitivity (no false negatives) and 100% specificity (no false alarm).

The graph space is divided into 2 triangles by a diagonal (the so-called line of no-discrimination). As the size of the sample increases, a random classifier's ROC point migrates towards the diagonal line. Points above the diagonal represent good classification results (better than random), points below the line represent poor results (worse than random). 

## Exercise: 
Suppose you developped models and plotted their ROC points. Model 1 falls approximately on the line of no-discrimination. Model 2 lays way below the line, indicating a very low true positive rate. Which is the model seems to be more promissing?

	A2: The model that falls on the line is a model that makes random guesses. It has any knowledge about your data that improves the classification. You may as well just flip a coin. Model 2 makes consistently poor predictions. However, you could obtain a good predictor if you would simply invert its results. Hence, although model 2 is a very poor model, you may be able to learn something from it.  


Let's look into the prediction results of 3 different classfiers (A, B, C) from 100 positive and 100 negative instances, to apply what we just learned.

## Exercise: 
- Calculate the TRP and FPR for each of the 3 models and plot the corresponding ROC point onto the graph. 
- Which one is the model you would choose?


# Curves in ROC Space

So far, we have only considered single points in the ROC space. However, in reality a ROC curve is often used to assess the model quality.

In binary classification, the class label is often determined based on a continuous variable X, which is a "score" computed for the instance. Given a threshold parameter T, the instance is classified as "positive" if X>T, and "negative" otherwise. The threshold T allows the experimenter to adjust the threshold and calculate for each T a new ROC point. By repetition a curve can be obtained. 

For instance, a very simple, univariate classifier could look only at the blood pressure to determine whether a individual is healthy. Let the initial threshold T be 140/90mmHg. If measure blood pressure X falls above T, the person is labelled as "positive". Instances with a pressure below 2 g/dL are classified as "negative". Each change of the threshold will lead to a different point in the ROC space. 

## Exercise: 
- Suppose you choose a ridicilously high threshold T. How would it influence the TPR and FPR of your classifier? Where would the ROC point be located? 
- Suppose you choose a ridicilously low threshold T. How would it influence the TPR and FPR of your classifier? Where would the ROC point be located? 

So, what is the point of creating ROC curves? Firstly,  an optimal can be dertermined. 
So far we have seen that the ROC curve allows us to choose the optimal threshold for a given classifier. An additional application could be to plot ROC curves of other classifier and compare their curves against each other. 

http://www.kovcomp.co.uk/support/XL-Tut/life-ROC-curves-receiver-operating-characteristic.html

 








