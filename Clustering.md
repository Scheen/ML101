# Clustering the digits dataset

In this tutorial, we will apply K-Means clustering. k-means aims to partition n observations into k clusters in which each observation belongs to the cluster with the nearest mean. 

After initializing k means at random, the algorithm proceeds by alternating between two steps: 
1. Assignment step: Assign each observation to the cluster whose mean has the least squared Euclidean distance
2. Update step: Calculate the new means to be the centroids of the observations in the new clusters.

The means will eventually move otwards the center of the clusters. The algorithm has converged when the assignments no longer change. 

Note: There is no guarantee that the optimum is found using this algorithm. Also, the users needs to come up with k.

## Step 0: Import packages 
Before we start, we need to load a couple of packages that we will need throughout the exercise.

```python
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score, confusion_matrix
from scipy.stats import mode
import matplotlib.pyplot as plt
import seaborn as sns # Seaborn is a visualization library based on matplotlib.
from sklearn.manifold import TSNE # a tool to visualize high-dimensional data
import numpy as np
import warnings; warnings.simplefilter('ignore')
```

## Step 1:  Load your data
```python
from sklearn.datasets import load_digits
digits = load_digits()
```

## Step 2: Take look at your data
Let's try to get a feeling for the data we just loaded.
```python
digits.data.shape
```

## Step 3: Clustering
We already imported the kmeans algorithm at the very beginning of the script (i.e. KMEANS). Next to a number of default parameters, it requires one 1 obligatory parameter: 'n_clusters'. It represents the number of clusters or groups we expect to find within the data. 
```python
kmeans = KMeans(n_clusters=10, random_state=0)
clusters = kmeans.fit_predict(digits.data)
kmeans.cluster_centers_.shape
```

## Step 5: Model Evaluation
```python
fig, ax = plt.subplots(2, 5, figsize=(8, 3))
centers = kmeans.cluster_centers_.reshape(10, 8, 8)
for axi, center in zip(ax.flat, centers):
    axi.set(xticks=[], yticks=[])
    axi.imshow(center, interpolation='nearest', cmap=plt.cm.binary)
```

```python
labels = np.zeros_like(clusters)
for i in range(10):
    mask = (clusters == i)
    labels[mask] = mode(digits.target[mask])[0]
```

```python
accuracy_score(digits.target, labels)
```

```python
mat = confusion_matrix(digits.target, labels)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=digits.target_names,
            yticklabels=digits.target_names)
plt.xlabel('true label')
plt.ylabel('predicted label');
```

##### Project the data: this step will take several seconds
```python
tsne = TSNE(n_components=2, init='random', random_state=0)
digits_proj = tsne.fit_transform(digits.data)
```

##### Compute the clusters
```python
kmeans = KMeans(n_clusters=10, random_state=0)
clusters = kmeans.fit_predict(digits_proj)
```

#####  Permute the labels
```python
labels = np.zeros_like(clusters)
for i in range(10):
    mask = (clusters == i)
    labels[mask] = mode(digits.target[mask])[0]
```

##### Compute the accuracy
```python
accuracy_score(digits.target, labels)
```

# Color Quantization using K-Means

Performs a pixel-wise Vector Quantization (VQ) of an image of the summer palace in China. The objective is to reduce the number of colors required to show the image, while preserving the overall appearance quality.

## Step 0: Import packages 
```python
from sklearn.cluster import MiniBatchKMeans
```

## Step 1:  Load your data
```python
from sklearn.datasets import load_sample_image
china = load_sample_image("china.jpg")
```

## Step 2: Take look at your data
```python
ax = plt.axes(xticks=[], yticks=[])
ax.imshow(china);
```

## Step 3: Data pre-processing
```python
data = china / 255.0 # use 0...1 scale
data = data.reshape(427 * 640, 3)
data.shape
```

```python
def plot_pixels(data, title, colors=None, N=10000):
    if colors is None:
        colors = data
    
    # choose a random subset
    rng = np.random.RandomState(0)
    i = rng.permutation(data.shape[0])[:N]
    colors = colors[i]
    R, G, B = data[i].T
    
    fig, ax = plt.subplots(1, 2, figsize=(16, 6))
    ax[0].scatter(R, G, color=colors, marker='.')
    ax[0].set(xlabel='Red', ylabel='Green', xlim=(0, 1), ylim=(0, 1))

    ax[1].scatter(R, B, color=colors, marker='.')
    ax[1].set(xlabel='Red', ylabel='Blue', xlim=(0, 1), ylim=(0, 1))

    fig.suptitle(title, size=20);
    plot_pixels(data, title='Input color space: 16 million possible colors')
```

## Step 4: Clustering
```python
kmeans = MiniBatchKMeans(16)
kmeans.fit(data)
new_colors = kmeans.cluster_centers_[kmeans.predict(data)]

plot_pixels(data, colors=new_colors,
            title="Reduced color space: 16 colors")
```

## Step 5: Model Evaluation
```python
china_recolored = new_colors.reshape(china.shape)

fig, ax = plt.subplots(1, 2, figsize=(16, 6),
                       subplot_kw=dict(xticks=[], yticks=[]))
fig.subplots_adjust(wspace=0.05)
ax[0].imshow(china)
ax[0].set_title('Original Image', size=16)
ax[1].imshow(china_recolored)
ax[1].set_title('16-color Image', size=16);
```

